# DigiBack - Wiki

[Home](https://gitlab.com/Stellucidam/DigiBack_java_backend/wiki)

## API
[Endpoints](https://gitlab.com/Stellucidam/DigiBack_java_backend/wiki/API.Endpoints)

[Objects](https://gitlab.com/Stellucidam/DigiBack_java_backend/wiki/API.Objects)
