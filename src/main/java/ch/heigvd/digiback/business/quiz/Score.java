package ch.heigvd.digiback.business.quiz;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Score {
    private int score;
    private int total;
}

