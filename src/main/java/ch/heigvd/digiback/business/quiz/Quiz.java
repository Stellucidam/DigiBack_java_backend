package ch.heigvd.digiback.business.quiz;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode
public class Quiz {
    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idQuiz;

    @Getter
    @Column(unique = true)
    private String title;

    @Getter
    @OneToMany
    private List<Question> questions;
}
