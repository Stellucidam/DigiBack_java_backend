package ch.heigvd.digiback.business.quiz.credential;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class QuizTitleCredential {
    private Long idQuiz;
    private String title;
}
