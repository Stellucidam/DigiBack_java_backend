package ch.heigvd.digiback.business.quiz;

import ch.heigvd.digiback.business.quiz.credential.*;
import ch.heigvd.digiback.business.user.User;
import ch.heigvd.digiback.business.user.UserRepository;
import ch.heigvd.digiback.error.exception.UnknownObjectException;
import ch.heigvd.digiback.error.exception.WrongCredentialsException;
import ch.heigvd.digiback.status.Status;
import ch.heigvd.digiback.status.StatusType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/quiz")
public class QuizController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AnsweredQuizRepository answeredQuizRepository;

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @PostMapping("/upload")
    public Status uploadQuiz(@RequestBody List<QuizCredential> quizCredentials) {
        try {
            quizCredentials.forEach(quizCredential -> {
                List<Question> questions = new LinkedList<>();
                quizCredential.getQuestions().forEach(question -> {
                    questions.add(
                            questionRepository.saveAndFlush(Question.builder()
                                    .title(question)
                                    .build()));
                });

                quizRepository.saveAndFlush(Quiz.builder()
                        .title(quizCredential.getTitle())
                        .questions(questions)
                        .build());
            });
            return Status.builder()
                    .status(StatusType.SUCCESS)
                    .message("The quiz were added successfully.")
                    .build();
        } catch (Exception e) {
            return Status.builder()
                    .status(StatusType.FAIL)
                    .message(e.getMessage())
                    .build();
        }
    }


    @PostMapping("/user/{idUser}")
    public Status answerQuestion(
            @RequestParam(name = "token") String token,
            @PathVariable(name = "idUser") Long idUser,
            @RequestBody AnsweredQuestion answeredQuestion) {
        try {
            User authenticated = findVerifiedUserByIdAndToken(idUser, token)
                    .orElseThrow(WrongCredentialsException::new);
            AtomicBoolean quizExists = new AtomicBoolean(false);
            Question question = questionRepository.findById(answeredQuestion.getIdQuestion()).orElse(null);
            if (question == null) {
                throw new UnknownObjectException("The question id does not exist");
            }
            authenticated.getAnsweredQuizList().forEach(quiz -> {
                if (quiz.getIdQuiz().equals(answeredQuestion.getIdQuiz())) {
                    quiz.getAnswerMap().put(question, answeredQuestion.getAnswer());
                    answeredQuizRepository.saveAndFlush(quiz);
                    quizExists.set(true);
                }
            });

            if (!quizExists.get()) {
                Quiz quiz = quizRepository.findById(answeredQuestion.getIdQuiz()).orElse(null);
                if (quiz == null) {
                    throw new UnknownObjectException("The quiz id does not exist");
                }
                Map<Question, QuestionAnswer> answerMap = new HashMap<>();
                answerMap.put(question, answeredQuestion.getAnswer());
                authenticated.getAnsweredQuizList().add(answeredQuizRepository.saveAndFlush(AnsweredQuiz.builder()
                        .idQuiz(quiz.getIdQuiz())
                        .answerMap(answerMap)
                        .build()));
            }

            userRepository.saveAndFlush(authenticated);

        } catch (WrongCredentialsException | UnknownObjectException e) {
            return Status.builder()
                    .status(StatusType.FAIL)
                    .message(e.getMessage())
                    .build();
        }
        return Status.builder()
                .status(StatusType.SUCCESS)
                .message("Your answer has been noted.")
                .build();
    }

    @GetMapping("")
    public List<QuizTitleCredential> getQuiz() {
        List<QuizTitleCredential> quizTitleCredentials = new LinkedList<>();
        quizRepository.findAll().forEach(quiz -> quizTitleCredentials.add(QuizTitleCredential.builder()
                .idQuiz(quiz.getIdQuiz())
                .title(quiz.getTitle())
                .build()));
        return quizTitleCredentials;
    }

    @GetMapping("/{idQuiz}")
    public QuizWithQuestionsCredential getQuiz(@PathVariable(name = "idQuiz") Long idQuiz) {
        Quiz quiz = quizRepository.findById(idQuiz).orElse(Quiz.builder().build());
        List<Question> questions = new LinkedList<>();
        quiz.getQuestions().forEach(question -> {
            questions.add(Question.builder()
                    .idQuestion(question.getIdQuestion())
                    .title(question.getTitle())
                    .build());
        });
        return QuizWithQuestionsCredential.builder()
                .title(quiz.getTitle())
                .questions(questions)
                .build();
    }

    @GetMapping("/{idQuiz}/answers/user/{idUser}")
    public List<QuestionAnswerCredential> getQuizAnswers(
            @RequestParam(name = "token") String token,
            @PathVariable(name = "idUser") Long idUser,
            @PathVariable(name = "idQuiz") Long idQuiz) {
        List<QuestionAnswerCredential> answers = new LinkedList<>();
        try {
            User authenticated = findVerifiedUserByIdAndToken(idUser, token)
                    .orElseThrow(WrongCredentialsException::new);
            Quiz quiz = quizRepository.findById(idQuiz).orElse(null);
            AtomicReference<AnsweredQuiz> answeredQuiz = new AtomicReference<>();
            authenticated.getAnsweredQuizList().forEach(answeredQuizFound -> {
                if (answeredQuizFound.getIdQuiz().equals(idQuiz)) {
                    answeredQuiz.set(answeredQuizFound);
                }
            });

            if (answeredQuiz.get() != null && quiz != null) {
                List<Long> ids = new LinkedList<>();
                answeredQuiz.get().getAnswerMap().forEach((question, answer) -> {
                    answers.add(QuestionAnswerCredential.builder()
                            .answer(answer)
                            .idQuestion(question.getIdQuestion())
                            .build());
                    ids.add(question.getIdQuestion());
                });

                quiz.getQuestions().forEach(question -> {
                    if (!ids.contains(question.getIdQuestion())) {
                        answers.add(QuestionAnswerCredential.builder()
                                .answer(QuestionAnswer.NOT_SURE)
                                .idQuestion(question.getIdQuestion())
                                .build());
                    }
                });
                answeredQuizRepository.saveAndFlush(answeredQuiz.get());
            } else {
                return null;
            }

        } catch (WrongCredentialsException e) {
            e.printStackTrace();
            return null;
        }

        return answers;
    }

    @GetMapping("/{idQuiz}/score/user/{idUser}")
    public Score getQuizScore(
            @RequestParam(name = "token") String token,
            @PathVariable(name = "idUser") Long idUser,
            @PathVariable(name = "idQuiz") Long idQuiz) {
        AtomicInteger
                score = new AtomicInteger(0),
                total = new AtomicInteger(0);
        try {
            User authenticated = findVerifiedUserByIdAndToken(idUser, token)
                    .orElseThrow(WrongCredentialsException::new);
            Quiz quiz = quizRepository.findById(idQuiz).orElse(null);
            AtomicReference<AnsweredQuiz> answeredQuiz = new AtomicReference<>();
            authenticated.getAnsweredQuizList().forEach(answeredQuizFound -> {
                if (answeredQuizFound.getIdQuiz().equals(idQuiz)) {
                    answeredQuiz.set(answeredQuizFound);
                }
            });

            if (answeredQuiz.get() != null && quiz != null) {
                if (quiz.getTitle().equals("STarT Back Screening Tool")) {
                    answeredQuiz.get().getAnswerMap().forEach((question, answer) -> {
                        // Total
                        total.addAndGet(answer.getStateScore());
                        // Subscore
                        if (question.getIdQuestion() >= 4) {
                            score.addAndGet(QuestionAnswer.TRUE.getScore());
                        }
                    });
                } else {
                    List<Long> ids = new LinkedList<>();
                    answeredQuiz.get().getAnswerMap().forEach((question, answer) -> {
                        ids.add(question.getIdQuestion());
                        if (question.getTitle().equals("Se pencher est bon pour votre dos") ||
                                question.getTitle().equals("Si vous avez mal au dos, vous devriez essayer de rester actif(ve)")) {
                            score.addAndGet(answer.getInvertedScore());
                            total.addAndGet(QuestionAnswer.TRUE.getInvertedScore());
                        } else {
                            score.addAndGet(answer.getScore());
                            total.addAndGet(QuestionAnswer.TRUE.getScore());
                        }
                    });

                    quiz.getQuestions().forEach(question -> {
                        if (!ids.contains(question.getIdQuestion())) {
                            answeredQuiz.get().getAnswerMap().put(question, QuestionAnswer.NOT_SURE);
                            score.addAndGet(QuestionAnswer.NOT_SURE.getScore());
                            total.addAndGet(QuestionAnswer.FALSE.getScore());
                        }
                    });
                }
                answeredQuizRepository.saveAndFlush(answeredQuiz.get());
            } else {
                return null;
            }

        } catch (WrongCredentialsException e) {
            e.printStackTrace();
            return null;
        }

        return Score.builder()
                .score(score.get())
                .total(total.get())
                .build();
    }

    private Optional<User> findVerifiedUserByIdAndToken(
            Long idUser,
            String token
    ) {
        return userRepository.findByToken(token)
                .filter(user -> user.getIdUser().equals(idUser));
    }
}
