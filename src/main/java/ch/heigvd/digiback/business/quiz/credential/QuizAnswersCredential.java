package ch.heigvd.digiback.business.quiz.credential;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class QuizAnswersCredential {
    // Map<Long, QuestionAnswer>
    private List<QuestionAnswerCredential> answers;
}
