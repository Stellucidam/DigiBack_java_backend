package ch.heigvd.digiback.business.quiz;

import lombok.*;

import javax.persistence.*;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode
public class AnsweredQuiz {
    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idAnsweredQuiz;

    @Getter
    private Long idQuiz;

    @Getter
    @ElementCollection
    private Map<Question, QuestionAnswer> answerMap;
}
