package ch.heigvd.digiback.business.quiz;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AnsweredQuizRepository extends JpaRepository<AnsweredQuiz, Long> {
}
