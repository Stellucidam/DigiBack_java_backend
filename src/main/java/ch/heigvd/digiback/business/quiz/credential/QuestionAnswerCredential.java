package ch.heigvd.digiback.business.quiz.credential;

import ch.heigvd.digiback.business.quiz.QuestionAnswer;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class QuestionAnswerCredential {
    private Long idQuestion;
    private QuestionAnswer answer;
}
