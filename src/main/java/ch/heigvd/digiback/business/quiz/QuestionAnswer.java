package ch.heigvd.digiback.business.quiz;

import lombok.Getter;

public enum QuestionAnswer {
    FALSE(1),
    MAYBE_FALSE(2),
    NOT_SURE(3),
    MAYBE_TRUE(4),
    TRUE(5);

    @Getter
    private final int score;
    QuestionAnswer(int score) {
        this.score = score;
    }

    public int getInvertedScore() {
        switch (this) {
            case TRUE:
                return 1;
            case MAYBE_TRUE:
                return 2;
            case NOT_SURE:
                return 3;
            case MAYBE_FALSE:
                return 4;
            case FALSE:
                return 5;
        }
        return 0;
    }

    public int getStateScore() {
        switch (this) {
            case TRUE:
            case MAYBE_TRUE:
                return 1;
            case NOT_SURE:
            case MAYBE_FALSE:
            case FALSE:
                return 0;
        }
        return 0;
    }
}
