package ch.heigvd.digiback.business.quiz;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class AnsweredQuestion {
    private Long idQuestion;
    private Long idQuiz;
    private QuestionAnswer answer;
}
