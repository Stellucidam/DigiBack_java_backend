package ch.heigvd.digiback.business.quiz.credential;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class QuizCredential {
    private String title;
    private List<String> questions;
}
