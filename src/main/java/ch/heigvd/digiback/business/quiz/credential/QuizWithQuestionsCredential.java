package ch.heigvd.digiback.business.quiz.credential;

import ch.heigvd.digiback.business.quiz.Question;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class QuizWithQuestionsCredential {
    private String title;
    private List<Question> questions;
}
