package ch.heigvd.digiback.business.exercise.credential;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ExerciseTitleCredential {
    private Long idExercise;
    private String title;
    private String imageURL;
}
