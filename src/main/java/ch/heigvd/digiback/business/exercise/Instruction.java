package ch.heigvd.digiback.business.exercise;

import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode
public class Instruction {
    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idInstruction;

    @Getter
    private int position;

    @Getter
    @Column(length = 5000)
    private String instruction;

    @Getter
    private String title;
}
