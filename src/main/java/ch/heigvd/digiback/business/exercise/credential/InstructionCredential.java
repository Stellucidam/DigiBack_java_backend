package ch.heigvd.digiback.business.exercise.credential;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class InstructionCredential {
    private int position;
    private String instruction;
    private String title;
}
