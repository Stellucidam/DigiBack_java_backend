package ch.heigvd.digiback.business.exercise;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode
public class Exercise {
    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idExercise;

    @Getter
    private String imageURL;

    @Getter
    private String link;

    @Getter
    private String title;

    @Getter
    @OneToMany
    private List<Instruction> instructions;
}
