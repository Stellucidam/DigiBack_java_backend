package ch.heigvd.digiback.business.exercise;

import ch.heigvd.digiback.business.exercise.credential.ExerciseCredential;
import ch.heigvd.digiback.business.exercise.credential.ExerciseTitleCredential;
import ch.heigvd.digiback.business.exercise.credential.InstructionCredential;
import ch.heigvd.digiback.business.user.UserRepository;
import ch.heigvd.digiback.status.Status;
import ch.heigvd.digiback.status.StatusType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/exercise")
public class ExerciseController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ExerciseRepository exerciseRepository;

    @Autowired
    private InstructionRepository instructionRepository;

    @PostMapping("/upload")
    public Status uploadExercises(@RequestBody List<ExerciseCredential> exercises) {
        try {
            exercises.forEach(exerciseCredential -> {
                List<Instruction> instructions = new LinkedList<>();
                exerciseCredential.getInstructions().forEach(instructionCredential -> {
                    instructions.add(
                            instructionRepository.saveAndFlush(Instruction.builder()
                                    .instruction(instructionCredential.getInstruction())
                                    .position(instructionCredential.getPosition())
                                    .title(instructionCredential.getTitle())
                                    .build()));
                });

                exerciseRepository.saveAndFlush(Exercise.builder()
                        .imageURL(exerciseCredential.getImageURL())
                        .link(exerciseCredential.getLink())
                        .title(exerciseCredential.getTitle())
                        .instructions(instructions)
                        .build());
            });
            return Status.builder()
                    .status(StatusType.SUCCESS)
                    .message("The exercises were added successfully.")
                    .build();
        } catch (Exception e) {
            return Status.builder()
                    .status(StatusType.FAIL)
                    .message(e.getMessage())
                    .build();
        }
    }

    @GetMapping("")
    public List<ExerciseTitleCredential> getExercises() {
        List<ExerciseTitleCredential> exercises = new LinkedList<>();
        exerciseRepository.findAll().forEach(exercise -> {
            exercises.add(ExerciseTitleCredential.builder()
                    .idExercise(exercise.getIdExercise())
                    .imageURL(exercise.getImageURL())
                    .title(exercise.getTitle())
                    .build());
        });
        return exercises;
    }

    @GetMapping("/{idExercise}")
    public ExerciseCredential getExercise(@PathVariable(name = "idExercise") Long idExercise) {
        Exercise exercise = exerciseRepository.findById(idExercise).orElse(Exercise.builder().build());
        List<InstructionCredential> instructions = new LinkedList<>();
        exercise.getInstructions().forEach(instruction -> {
            instructions.add(InstructionCredential.builder()
                    .instruction(instruction.getInstruction())
                    .position(instruction.getPosition())
                    .title(instruction.getTitle())
                    .build());
        });
        return ExerciseCredential.builder()
                .imageURL(exercise.getImageURL())
                .instructions(instructions)
                .link(exercise.getLink())
                .title(exercise.getTitle())
                .build();
    }
}
