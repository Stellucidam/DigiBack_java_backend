package ch.heigvd.digiback.business.exercise.credential;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class ExerciseCredential {
    private String imageURL;
    private String link;
    private String title;
    private List<InstructionCredential> instructions;
}
