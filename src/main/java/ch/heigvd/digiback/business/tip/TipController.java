package ch.heigvd.digiback.business.tip;

import ch.heigvd.digiback.business.activity.Activity;
import ch.heigvd.digiback.business.activity.ActivityRepository;
import ch.heigvd.digiback.business.statistic.Stat;
import ch.heigvd.digiback.business.statistic.StatisticController;
import ch.heigvd.digiback.business.user.User;
import ch.heigvd.digiback.business.user.UserRepository;
import ch.heigvd.digiback.error.exception.WrongCredentialsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/tip")
public class TipController {
    private static final int DEFAULT_PERIOD = 5;
    private static final int DEFAULT_EXERCISE_NBR = 3;
    private static final int DEFAULT_STEPS_NBR = 10000;
    private static final int DEFAULT_QUIZ_NBR = 3;

    @Autowired
    private StatisticController statisticController;

    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/user/{idUser}")
    public List<Tip> getTip(
            @RequestParam(name = "token") String token,
            @PathVariable(name = "idUser") Long idUser)
            throws WrongCredentialsException {
        return getTips(token, idUser, DEFAULT_PERIOD);
    }

    @GetMapping("/user/{idUser}/days/{daysNbr}")
    public List<Tip> getTipForGivenPeriod(
            @RequestParam(name = "token") String token,
            @PathVariable(name = "idUser") Long idUser,
            @PathVariable(name = "daysNbr") int daysNbr)
            throws WrongCredentialsException {
        return getTips(token, idUser, daysNbr);
    }

    private List<Tip> getTips(String token, Long idUser, int daysNbr)
            throws WrongCredentialsException {
        Stat userStat = statisticController.getStatsForUserAndDays(token, idUser, daysNbr);
        User authenticated = findVerifiedUserByIdAndToken(idUser, token)
                .orElseThrow(WrongCredentialsException::new);
        Activity lastActivity = activityRepository
                .findByUserAndDate(authenticated, new Date(new java.util.Date().getTime()))
                .orElse(Activity.builder()
                        .date(new Date(new java.util.Date().getTime()))
                        .exercises(new LinkedList<>())
                        .nbrSteps(0L)
                        .nbrQuiz(0L)
                        .user(authenticated)
                        .build());
        return getTipsFromStatAndLastActivity(userStat, lastActivity);
    }

    private Optional<User> findVerifiedUserByIdAndToken(
            Long idUser,
            String token
    ) {
        return userRepository.findByToken(token)
                .filter(user -> user.getIdUser().equals(idUser));
    }

    public List<Tip> getTipsFromStatAndLastActivity(Stat stat, Activity lastActivity) {
        List<Tip> tips = new LinkedList<>();
        // Get tips from activity
        if (lastActivity.getExercises().size() < DEFAULT_EXERCISE_NBR) {
            tips.add(Tip.builder()
                    .type(TipType.EXERCISE)
                    .repetition(DEFAULT_EXERCISE_NBR - lastActivity.getExercises().size())
                    .build());
        }
        if (lastActivity.getNbrQuiz() < DEFAULT_QUIZ_NBR) {
            tips.add(Tip.builder()
                    .type(TipType.QUIZ)
                    .repetition((int) (DEFAULT_QUIZ_NBR - lastActivity.getNbrQuiz()))
                    .build());
        }
        if (lastActivity.getNbrSteps() < DEFAULT_STEPS_NBR) {
            tips.add(Tip.builder()
                    .type(TipType.WALK)
                    .repetition((int) (DEFAULT_STEPS_NBR - lastActivity.getNbrSteps()))
                    .build());
        }

        // Get tips from stats
        // todo utiliser les valeurs d'un dos sain pour faire les comparaisons

        return tips;
    }
}
