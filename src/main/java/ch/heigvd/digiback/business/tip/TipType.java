package ch.heigvd.digiback.business.tip;

public enum TipType {
    WALK,
    STRETCH,
    MUSCLE,
    EXERCISE,
    MOVEMENT_EXERCISE,
    STILL_EXERCISE,
    QUIZ;
}
