package ch.heigvd.digiback.business.statistic;

import ch.heigvd.digiback.business.movement.Movement;
import ch.heigvd.digiback.business.movement.MovementRepository;
import ch.heigvd.digiback.business.movement.MovementType;
import ch.heigvd.digiback.business.user.User;
import ch.heigvd.digiback.business.user.UserRepository;
import ch.heigvd.digiback.error.exception.WrongCredentialsException;
import ch.heigvd.digiback.python.PythonRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/stat")
public class StatisticController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MovementRepository movementRepository;

    @GetMapping("/")
    public Stat getStats() {
        String stat = "";
        try {
            return Stat.builder()
                    .stat(PythonRunner.run())
                    .build();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Stat.builder().stat("ERROR").build();
    }

    @GetMapping("/user/{idUser}")
    public Stat getStatsForUserAndDays(
            @RequestParam(name = "token") String token,
            @PathVariable(name = "idUser") Long idUser)
            throws WrongCredentialsException {
        LinkedList<Movement> movements =  movementRepository
                .findByUser(
                    findVerifiedUserByIdAndToken(idUser, token).orElseThrow(WrongCredentialsException::new)
                );
        Map<MovementType, Stat> statByMovementType = getPainAvgByMovementType(token, idUser);
        Stat stat = getStat(movements);
        stat.setStatByMovementType(statByMovementType);
        return stat;
    }

    @GetMapping("/user/{idUser}/days/{daysNbr}")
    public Stat getStatsForUserAndDays(
            @RequestParam(name = "token") String token,
            @PathVariable(name = "idUser") Long idUser,
            @PathVariable(name = "daysNbr") int daysNbr)
            throws WrongCredentialsException {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -daysNbr);
        LinkedList<Movement> movements =  movementRepository
                .findByUserAndDateIsAfter(
                    findVerifiedUserByIdAndToken(idUser, token).orElseThrow(WrongCredentialsException::new),
                    new Date(cal.getTime().getTime())
                );
        Map<MovementType, Stat> statByMovementType = getPainAvgByMovementType(token, idUser, new Date(cal.getTime().getTime()));
        Stat stat = getStat(movements);
        stat.setStatByMovementType(statByMovementType);
        return stat;
    }

    private Stat getStat(LinkedList<Movement> movements) {
        // Angle information
        AtomicReference<Float> highestAngle = new AtomicReference<>((float) 0);
        AtomicReference<Float> anglesSum = new AtomicReference<>((float) 0);
        AtomicReference<Float> nbrAngles = new AtomicReference<>((float) 0);
        List<DatedAngle> angleEvolution = new LinkedList<>();

        // Pain information
        AtomicReference<Float> painSum = new AtomicReference<>((float) 0);
        AtomicReference<Float> nbrPain = new AtomicReference<>((float) 0);
        List<DatedPain> painEvolution = new LinkedList<>();

        movements.forEach(movement -> {
            // Find the highest angle in the movement
            AtomicReference<Float> highestMAngle = new AtomicReference<>(movement.getAngles().get(0).getAngle());
            // For each angle in the movement
            movement.getAngles().forEach(angle -> {
                // Find the highest angle in general
                if (Math.abs(angle.getAngle()) > Math.abs(highestAngle.get())) {
                    highestAngle.set(angle.getAngle());
                }
                if (Math.abs(angle.getAngle()) > Math.abs(highestMAngle.get())) {
                    highestMAngle.set(angle.getAngle());
                }
                // Find the number of angles
                anglesSum.set(anglesSum.get() + highestMAngle.get());
                nbrAngles.set(nbrAngles.get() + 1);
            });
            angleEvolution.add(DatedAngle.builder()
                    .date(movement.getDate())
                    .value(highestAngle.get())
                    .build());

            // Save the pain level
            if (movement.getPain() != null) {
                nbrPain.set(nbrPain.get() + 1);
                painSum.set(painSum.get() + movement.getPain().getLevel());
                painEvolution.add(DatedPain.builder()
                        .date(movement.getDate())
                        .value(movement.getPain().getLevel())
                        .build());
            }
        });

        return Stat.builder()
                .highestAngle(highestAngle.get())
                .angleAverage(anglesSum.get() / nbrAngles.get())
                .angleEvolution(angleEvolution)
                .painEvolution(painEvolution)
                .painAverage(painSum.get() / nbrPain.get())
                .build();
    }

    private Map<MovementType, Stat> getPainAvgByMovementType(String token, Long idUser, Date afterDate) {
        Map<MovementType, Stat> map = new HashMap<>();

        MovementType.getTypes().forEach(type -> {
            try {
                map.put(type, getStat(movementRepository
                        .findByTypeAndDateIsAfterAndUser(
                                type,
                                afterDate,
                                findVerifiedUserByIdAndToken(idUser, token).orElseThrow(WrongCredentialsException::new)
                        )));
            } catch (WrongCredentialsException e) {
                e.printStackTrace();
            }
        });

        return map;
    }

    private Map<MovementType, Stat> getPainAvgByMovementType(String token, Long idUser) {
        Map<MovementType, Stat> map = new HashMap<>();

        MovementType.getTypes().forEach(type -> {
            try {
                map.put(type, getStat(movementRepository
                        .findByTypeAndUser(
                                type,
                                findVerifiedUserByIdAndToken(idUser, token).orElseThrow(WrongCredentialsException::new)
                        )));
            } catch (WrongCredentialsException e) {
                e.printStackTrace();
            }
        });

        return map;
    }

    private Optional<User> findVerifiedUserByIdAndToken(Long idUser, String token) {
        return userRepository.findByToken(token).filter(user -> user.getIdUser().equals(idUser));
    }
}
