package ch.heigvd.digiback.business.statistic;

import lombok.Builder;
import lombok.Getter;

import java.sql.Date;

@Getter
@Builder
public class DatedAngle {
    private Date date;
    private float value;
}
