package ch.heigvd.digiback.business.statistic;

import ch.heigvd.digiback.business.movement.MovementType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Builder
public class Stat {
    private String stat;

    // Angle information
    private float highestAngle;
    private float angleAverage;
    private List<DatedAngle> angleEvolution;

    // Pain information
    private float painAverage;
    private List<DatedPain> painEvolution;

    @Setter
    private Map<MovementType, Stat> statByMovementType;
}
