package ch.heigvd.digiback.business.activity.credential;

import lombok.Builder;
import lombok.Data;

import java.sql.Date;
import java.util.List;

@Builder
@Data
public class ActivityCredential {
    private Date date;
    private Long nbrSteps;
    private List<Long> exercices;
    private Long nbrQuiz;
}
