package ch.heigvd.digiback.business.activity;

import ch.heigvd.digiback.business.user.User;
import lombok.*;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode
@Table(uniqueConstraints = @UniqueConstraint(columnNames={"date", "user"}))
public class Activity {
    @Id
    @Getter
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idActivity;

    @Getter
    @Column(name = "date")
    private Date date;

    @Getter
    @Setter
    private Long nbrSteps;

    @Getter
    @Setter
    @ElementCollection
    private List<Long> exercises;

    @Getter
    @Setter
    private Long nbrQuiz;

    @Getter
    @ManyToOne
    @JoinColumn(name = "user")
    private User user;
}
