package ch.heigvd.digiback.business.activity.credential;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class StepCredential {
    private String date;
    private Long nbrSteps;
}
