package ch.heigvd.digiback.business.activity.credential;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@JsonComponent
public class ExerciseCredentialDeserializer extends JsonDeserializer<DoneExerciseCredential> {

    private static final Logger logger = LoggerFactory.getLogger(ExerciseCredentialDeserializer.class);
    @Override
    public DoneExerciseCredential deserialize(
            JsonParser jsonParser,
            DeserializationContext deserializationContext
    ) throws IOException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);

        String date = node.get("date").asText();
        List<Long> idExercises = new LinkedList<>();
        JsonNode idExercisesJSON = node.get("idExercises");
        for (int i = 0; i < idExercisesJSON.size(); ++i) {
            idExercises.add(idExercisesJSON.get(i).longValue());
        }

        logger.info("Deserialized exercise");
        return DoneExerciseCredential.builder()
                .date(date)
                .exercises(idExercises)
                .build();
    }
}
