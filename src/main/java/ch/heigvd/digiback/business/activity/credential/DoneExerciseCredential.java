package ch.heigvd.digiback.business.activity.credential;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class DoneExerciseCredential {
    private String date;
    private List<Long> exercises;
}
